# SICRED

Itens necessários para ambiente:


* Java 8
* Maven

**Cenário de Negócio**

Todo dia útil por volta das 6 horas da manhã um colaborador da retaguarda 
do Sicredi recebe e organiza as informações de contas para enviar ao 
Banco Central.
Todas agencias e cooperativas enviam arquivos Excel à Retaguarda.
Hoje o Sicredi já possiu mais de 4 milhões de contas ativas.
Esse usuário da retaguarda exporta manualmente os dados em um arquivo 
CSV para ser enviada para a Receita Federal, 
antes as 10:00 da manhã na abertura das agências.

Requisito:
Usar o "serviço da receita" (fake) para processamento automático do arquivo.

Funcionalidade:
0. Criar uma aplicação SprintBoot standalone.
   Exemplo: java -jar SincronizacaoReceita <input-file>
1. Processa um arquivo CSV de entrada com o formato abaixo.
2. Envia a atualização para a Receita
   através do serviço (SIMULADO pela classe ReceitaService).
3. Retorna um arquivo com o resultado do envio da atualização da Receita.
   Mesmo formato adicionando o resultado em uma nova coluna.


Formato CSV:

```
agencia;conta;saldo;status
0101;12225-6;100,00;A
0101;12226-8;3200,50;A
3202;40011-1;-35,12;I
3202;54001-2;0,00;P
3202;00321-2;34500,00;B
```
**Build**

Após instalar Java e Maven. Entrar via terminal no diretório /sincronizacaoReceita e executar o comando:

```
mvn clean install
```
O arquivo jar executável será gerado em sincronizacaoReceita/target/sincronizacaoReceita-0.0.1-SNAPSHOT.jar


**Executando**

Após executar o build, via terminal entrar no diretóiro target/ e executar o comando passando como parametro o caminho do arquivo csv:


```
java -jar sincronizacaoReceita-0.0.1-SNAPSHOT.jar <input-file>
```

A aplicação irá processar o arquivo e no mesmo diretório do arquivo irá escrever o resultado em em um diretório ./output


```
agencia;conta;saldo;status;processamento
3202;00321-2;34.500;B;Sucesso
3202;40011-1;-35,12;I;Sucesso
3202;54001-2;0;P;Sucesso
0101;12226-8;3.200,5;A;Sucesso
0101;12225-6;100;A;Sucesso


```

