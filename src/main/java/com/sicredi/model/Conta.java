package com.sicredi.model;

import java.text.NumberFormat;
import java.util.Locale;

public class Conta {
	private String agencia;
	private String conta;
	private Double saldo;
	private String status;
	private String processamento = "Enviando";
    
    public Conta() {
    	
    }
	public Conta(String agencia, String conta, Double saldo, String status) {
		this.agencia = agencia;
		this.conta = conta;
		this.saldo = saldo;
		this.status = status;
	}

	public String getAgencia() {
		return agencia;
	}

	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}

	public String getConta() {
		return conta;
	}
	
	public String getContaOnlyNumbers() {
		return conta.replace("-", "");
	}

	public void setConta(String conta) {
		this.conta = conta;
	}

	public Double getSaldo() {
		return saldo;
	}
	
	

	public String getStrSaldo() {
		NumberFormat nf = NumberFormat.getInstance(new Locale ("pt", "BR")); 
		return nf.format(this.saldo);
	}

	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getProcessamento() {
		return processamento;
	}
	public void setProcessamento(String erro) {
		this.processamento = erro;
	}
	@Override
	public String toString() {
		return "Conta [agencia=" + agencia + ", conta=" + conta + ", saldo=" + saldo + ", status=" + status + ", processamento="
				+ processamento + "]";
	}
}
