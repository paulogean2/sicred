package com.sicredi.service;

import java.io.File;

import com.sicredi.model.Conta;

public interface ProcessaResultadoConta {
	public void setTotalEnviado(Long qty);
	public void initCsv(File file);
	public void gravaResultado(Conta conta);

}
