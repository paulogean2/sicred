package com.sicredi.service;

import com.sicredi.model.Conta;

public interface SincronizaService {
	void sicronizaReceita(Conta conta, ProcessaResultadoConta callBackConta);
}
