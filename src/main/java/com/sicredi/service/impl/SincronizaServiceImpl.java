package com.sicredi.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.sicredi.model.Conta;
import com.sicredi.service.ProcessaResultadoConta;
import com.sicredi.service.ReceitaService;
import com.sicredi.service.SincronizaService;
@Service
public class SincronizaServiceImpl implements SincronizaService{
	@Autowired
	ReceitaService receitaService;
	Conta conta;
	@Async
	public void sicronizaReceita(Conta conta, ProcessaResultadoConta callBackConta) {
		try {
	

			boolean result = receitaService.atualizarConta(conta.getAgencia(), conta.getContaOnlyNumbers(),
					conta.getSaldo(), conta.getStatus());
			conta.setProcessamento("Sucesso");

			if (!result) {
				conta.setProcessamento("Agencia, Conta ou Status Inválido");
			}

		} catch (RuntimeException e) {
			conta.setProcessamento("Erro de processamento");
			e.printStackTrace();
		} catch (InterruptedException e) {
			conta.setProcessamento("Erro de processamento");
			e.printStackTrace();
		}
		callBackConta.gravaResultado(conta);
	}
	

}
