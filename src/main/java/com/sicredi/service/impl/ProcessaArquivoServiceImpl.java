package com.sicredi.service.impl;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.opencsv.CSVReader;
import com.sicredi.model.Conta;
import com.sicredi.service.ProcessaArquivoService;
import com.sicredi.service.ProcessaResultadoConta;
import com.sicredi.service.SincronizaService;

@Service
public class ProcessaArquivoServiceImpl implements ProcessaArquivoService {
	private static final String OUTPUT_FOLDER = "output";

	@Autowired
	SincronizaService service;

	@Autowired
	ProcessaResultadoConta processaResultadoConta;
	File outputFile;

	public Long processaArquivo(File file) throws IOException {
		System.out.println("Read file: "+file);

		Long qnty = 0l;
		this.outputFile = toOutPut(file);
		CSVReader csvReader = new CSVReader(new FileReader(file), ';');
		String[] values = null;
		processaResultadoConta.initCsv(this.outputFile);
		while ((values = csvReader.readNext()) != null) {
			if (values[0].equals("agencia")) {
				continue;
			}
			qnty++;
			processaResultadoConta.setTotalEnviado(qnty);
			String strSaldo = values[2].replace(",", ".");
			Double saldo = Double.parseDouble(strSaldo);
			Conta conta = new Conta(values[0], values[1], saldo, values[3]);
			System.out.println("Enviando: " +conta.toString());
			service.sicronizaReceita(conta, processaResultadoConta);
		}
		System.out.println("Total Enviado: "+qnty);

		return qnty;

	}

	private File toOutPut(File file) {
		return new File(file.getParent() + File.separator + OUTPUT_FOLDER + File.separator + file.getName());
	}

}
