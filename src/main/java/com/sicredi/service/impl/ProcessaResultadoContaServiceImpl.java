package com.sicredi.service.impl;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.opencsv.CSVWriter;
import com.sicredi.model.Conta;
import com.sicredi.service.ProcessaResultadoConta;

@Service
public class ProcessaResultadoContaServiceImpl implements ProcessaResultadoConta {
    Logger log = LoggerFactory.getLogger(this.getClass().getName());

	File fileOutput;
	
	Long totalEnviado= 0l;
	Long totalProcessado= 0l;

	public void initCsv(File fileOut) {
		System.out.println("Output file: "+fileOut);
		this.fileOutput = fileOut;
		String[] values = new String[] { "agencia", "conta", "saldo", "status", "processamento" };
		try {
			File folder = new File(this.fileOutput.getParent());
			if (!folder.exists()) {
				folder.mkdirs();
			}
			if (fileOut.exists()) {
				fileOut.delete();
			}
			writeCSV(values, fileOut);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void gravaResultado(Conta conta) {
		totalProcessado ++;
		Double perc = (totalProcessado.floatValue()/this.totalEnviado.floatValue()) * 100.0;
		
		System.out.println("Processado: "+totalProcessado+" de " + this.totalEnviado  + " ("+perc.intValue()+"%) " +conta.toString());
		writeResult(conta);

	}

	public void writeResult(Conta conta) {
		String[] values = new String[] { conta.getAgencia(), conta.getConta(), conta.getStrSaldo(), conta.getStatus(),
				conta.getProcessamento() };
		try {
			writeCSV(values, this.fileOutput);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void writeCSV(String[] values, File file) throws IOException {
		FileWriter outputfile = new FileWriter(file, true);
		CSVWriter writer = new CSVWriter(outputfile, ';', CSVWriter.NO_QUOTE_CHARACTER,
				CSVWriter.DEFAULT_ESCAPE_CHARACTER, CSVWriter.DEFAULT_LINE_END);
		writer.writeNext(values);
		writer.close();
	}

	public void setTotalEnviado(Long qty) {
		this.totalEnviado = qty;
	}

}
