package com.sicredi.service;

import java.io.File;
import java.io.IOException;

public interface ProcessaArquivoService {
	Long processaArquivo(File file) throws IOException ;
}
