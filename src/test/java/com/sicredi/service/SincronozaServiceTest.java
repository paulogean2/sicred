package com.sicredi.service;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.sicredi.model.Conta;
import com.sicredi.sincronizacaoReceita.SincronizacaoReceitaApplication;
@ContextConfiguration(classes = SincronizacaoReceitaApplication.class)
@RunWith(SpringRunner.class)
public class SincronozaServiceTest {
	@Autowired
	private SincronizaService sincronizaService;
	
	@Autowired
	ProcessaResultadoConta processaResultadoConta;
	@Test
	public void sicronizaReceitaTest() throws RuntimeException, InterruptedException {
		Path currentRelativePath = Paths.get("asserts/arquivo_receita/output");
		File base = currentRelativePath.toAbsolutePath().toFile();

		File file=new File(base+File.separator+"teste_out.csv");
		processaResultadoConta.initCsv(file);
		Conta conta =new Conta();
		conta.setAgencia("101");
		conta.setConta("12225-6");
		conta.setSaldo(100.0);
		conta.setStatus("A");
		sincronizaService.sicronizaReceita(conta, processaResultadoConta);  
	}
}
