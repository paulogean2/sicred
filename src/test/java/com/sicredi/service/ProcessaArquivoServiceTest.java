package com.sicredi.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.sicredi.sincronizacaoReceita.SincronizacaoReceitaApplication;
@ContextConfiguration(classes = SincronizacaoReceitaApplication.class)
@RunWith(SpringRunner.class)
public class ProcessaArquivoServiceTest {
	@Autowired
	ProcessaArquivoService processaArquivoService;
	@MockBean
	private SincronizaService sincronizaService;
	@Test
	public void lerArquivo() throws IOException  {
		String nameFile="receita_simple.csv";
		Path currentRelativePath = Paths.get("asserts/arquivo_receita");
		File base = currentRelativePath.toAbsolutePath().toFile();
		System.out.println("Current relative path is: " + base); 
		Long qty =processaArquivoService.processaArquivo(new File(base+File.separator+nameFile));
		assertEquals(5, qty);

	}
}
